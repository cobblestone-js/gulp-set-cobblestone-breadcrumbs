gulp-set-cobblestone-breadcrumbs
================================

> A *gulp* plugin for creating a breadcrumb with parent metadata.

## Setup and usage

Install `gulp-set-cobblestone-breadcrumbs` using `npm`:

```sh
npm i gulp-set-cobblestone-breadcrumbs
```

In your `gulpfile.js`:

```js
var gulp = require('gulp'),
    setCobblestoneBreadcrumbs = require('gulp-set-cobblestone-breadcrumbs');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(setCobblestoneBreadcrumbs())
    .pipe(gulp.dest('./dest'));
});
```

A common workflow using this tool is to parse the files, then create a propery called `breadcrumbs` which has an ordered list of parent items, excluding the current one. This can then be walked in a template to write out the crumbs.

## Options

### property

*string*

Default: `page.breadcrumbs`

The name of the property (which can be nested) to put the resulting ordered list of breadcrumbs. The item contains the entire metadata (YAML) section for each parent going from the top-most to the most direct parent.
