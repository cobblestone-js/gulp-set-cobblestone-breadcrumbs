import * as _ from "lodash";

export class FileMetadata
{
    public isPushed: boolean;
    public parentPaths: string[] = [];

    constructor(
        public path: string,
        public data: any,
        public file: any)
    {
        this.isPushed = false;
        this.setParentRelativePaths();
    }

    public hasResolved(files: { [id: string]: FileMetadata })
    {
        // If we had no parents, then nothing to do.
        if (this.parentPaths.length == 0)
        {
            return true;
        }

        // Go through the parents and see if each one has been pushed.
        var parentsResolved = true;

        for (const parentPath of this.parentPaths)
        {
            // If we haven't seen the parent, then we have nothing to do.
            const parentMetadata = files[parentPath];

            if (!parentMetadata || !parentMetadata.isPushed)
            {
                parentsResolved = false;
                break;
            }
        }

        return parentsResolved;
    }

    public resolve(
        property: string,
        files: { [id: string]: FileMetadata })
    {
        // Build up the breadcrumb limitations.
        const breadcrumbs = this.parentPaths
            .map(p => files[p])
            .filter(p => !!p)
            .map(p => p.data);

        _.set(this.file, property, breadcrumbs);

        // DEBUG: console.log("resolve", this.path);
    }

    /**
     * Sets the internal paths to all the parent paths above this one
     * and in the right sequence. This is used to determine the
     * breadcrumbs and the order they are to be displayed.
     */
    private setParentRelativePaths(): void
    {
        var parts = this.path.replace(".html", "/").split('/').splice(1);
        parts.pop();

        for (const i in parts)
        {
            var parentPath = "/" + [... parts].splice(0, parseInt(i)).join("/") + "/";

            this.parentPaths.push(parentPath.replace("//", "/"));
        }

        // DEBUG: console.log("paths", this.path, this.parentPaths);
    }
}
