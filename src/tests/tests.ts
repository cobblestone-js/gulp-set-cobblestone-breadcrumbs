const plugin = require("../index");
import * as expect from "expect";
import "mocha";
import * as File from "vinyl";

describe("process files", function ()
{
    it("handles root file", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({ property: "data.breadcrumbs" });

        pipe.write(new File({ data: { id: 1, relativePath: "" } }));
        pipe.end();

        // Verify the results of the parsing.
        var output: number[] = [];

        pipe.on("data", function (file: any)
        {
            output.push(file.data && file.data.breadcrumbs
                ? file.data.breadcrumbs.map((b: any) => b.id)
                : undefined);
        });

        pipe.on("end", function ()
        {
            expect(output).toEqual([
                [],
            ]);
            done();
        });
    });

    it("handles one root child", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({ property: "data.breadcrumbs" });

        pipe.write(new File({ data: { id: 1, relativePath: "" } }));
        pipe.write(new File({ data: { id: 2, relativePath: "404.html" } }));
        pipe.end();

        // Verify the results of the parsing.
        var output: number[] = [];

        pipe.on("data", function (file: any)
        {
            output.push(file.data && file.data.breadcrumbs
                ? file.data.breadcrumbs.map((b: any) => b.id)
                : undefined);
        });

        pipe.on("end", function ()
        {
            expect(output).toEqual([
                [],
                [1],
            ]);
            done();
        });
    });

    it("handles one root child reversed", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({ property: "data.breadcrumbs" });

        pipe.write(new File({ data: { id: 2, relativePath: "404.html" } }));
        pipe.write(new File({ data: { id: 1, relativePath: "" } }));
        pipe.end();

        // Verify the results of the parsing.
        var output: number[] = [];

        pipe.on("data", function (file: any)
        {
            output.push(file.data && file.data.breadcrumbs
                ? file.data.breadcrumbs.map((b: any) => b.id)
                : undefined);
        });

        pipe.on("end", function ()
        {
            expect(output).toEqual([
                [],
                [1],
            ]);
            done();
        });
    });

    it("handles two root children", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({ property: "data.breadcrumbs" });

        pipe.write(new File({ data: { id: 2, relativePath: "404.html" } }));
        pipe.write(new File({ data: { id: 1, relativePath: "" } }));
        pipe.write(new File({ data: { id: 3, relativePath: "500.html" } }));
        pipe.end();

        // Verify the results of the parsing.
        var output: number[] = [];

        pipe.on("data", function (file: any)
        {
            output.push(file.data && file.data.breadcrumbs
                ? file.data.breadcrumbs.map((b: any) => b.id)
                : undefined);
        });

        pipe.on("end", function ()
        {
            expect(output).toEqual([
                [],
                [1],
                [1],
            ]);
            done();
        });
    });

    it("handles nested children", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({ property: "data.breadcrumbs" });

        pipe.write(new File({ data: { id: 2, relativePath: "a/404.html" } }));
        pipe.write(new File({ data: { id: 1, relativePath: "" } }));
        pipe.write(new File({ data: { id: 3, relativePath: "a/" } }));
        pipe.end();

        // Verify the results of the parsing.
        var output: number[] = [];

        pipe.on("data", function (file: any)
        {
            output.push(file.data && file.data.breadcrumbs
                ? file.data.breadcrumbs.map((b: any) => b.id)
                : undefined);
        });

        pipe.on("end", function ()
        {
            expect(output).toEqual([
                [],
                [1],
                [1, 3],
            ]);
            done();
        });
    });

    it("handles unresolvable", function (done)
    {
        // Pipe the files through the plugin.
        var pipe = plugin({ property: "data.breadcrumbs" });

        pipe.write(new File({ data: { id: 2, relativePath: "404.html" } }));
        pipe.end();

        // Verify the results of the parsing.
        var output: number[] = [];

        pipe.on("data", function (file: any)
        {
            output.push(file.data && file.data.breadcrumbs
                ? file.data.breadcrumbs.map((b: any) => b.id)
                : undefined);
        });

        pipe.on("end", function ()
        {
            expect(output).toEqual([[]]);
            done();
        });
    });
});
