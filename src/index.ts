import * as _ from "lodash";
import * as stream from "stream";
import * as through from "through2";
import { FileMetadata } from "./FileMetadata";

/**
 * Goes through and populates any files that are fully populated.
 */
function flushCompletedFiles(
    params: any,
    unpushed: string[],
    files: { [id: string]: FileMetadata },
    push: any,
): void
{
    // Go through all the files we know about and haven't pushed.
    for (var relativePath of unpushed)
    {
        // See if we have loaded all the parent items above this
        // one. If we have, then we can write them out.
        var file = files[relativePath];

        if (!file.hasResolved(files))
        {
            continue;
        }

        // This file is completely resolved, so fill it out.
        file.resolve(params.property, files);
        file.isPushed = true;
        _.remove(unpushed, f => f === relativePath);

        // DEBUG: console.log("pushing", relativePath);
        push(file.file);
    }
}

/**
 * Transforms individual files, gathering information for later process while
 * also attempting to populate the breadcrumbs as needed.
 */
function transform(
    params: any,
    unpushed: string[],
    files: { [id: string]: FileMetadata },
    file: any,
    callback: any,
    push: any,
): void
{
    // If we don't have a relative path, then we don't do anything.
    if (!file.data || file.data.relativePath === undefined)
    {
        return callback(null, file);
    }

    // Normalize the path to make it easier to figure out the paths.
    var relativePath = "/" + file.data.relativePath;

    // Copy the file's metadata and store it for later. We also remove the
    // contents to avoid a recursive processing.
    var data = { ...file.data };
    delete data.contents;
    delete data.breadcrumbs;

    files[relativePath] = new FileMetadata(relativePath, data, file);

    unpushed.push(relativePath);

    // Process breadcrumbs and flush out any files we've already processed.
    flushCompletedFiles(params, unpushed, files, push);

    // We just use an empty callback because `flushCompletedFiles` will write
    // out everything needed.
    callback(null);
}

/**
 * Called when the stream has finished and any missing files need to be flushed
 * out.
 *
 * @param done The callback after finishing the file.
 */
function flush(
    params: any,
    unpushed: string[],
    files: { [id: string]: FileMetadata },
    done: any,
    push: any,
): void
{
    // Go through the files and look for remaining ones.
    for (const path of unpushed)
    {
        const file = files[path];

        file.resolve(params.property, files);
        push(file.file);
    }

    // Finish up the flushing.
    done();
}

function process(params: any): stream.Transform
{
    // Parse and figure out all the parameters.
    if (!params)
    {
        params = {};
    }

    params.property = params.property || "page.breadcrumbs";

    // Keep track of every file, both the YAML metadata and every file that
    // current needs to have that value populated.
    var files: { [id: string]: FileMetadata } = {};
    var unpushed: string[] = [];

    // Return the resulting pipe.
    return through(
        { objectMode: true },
        function (f: any, e: any, c: any)
        {
            transform(params, unpushed, files, f, c, (a: any) => this.push(a));
        },
        function (c: any)
        {
            flush(params, unpushed, files, c, (a: any) => this.push(a));
        });
}

// Gulp wants a single function.
module.exports = process;
